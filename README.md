## OCC-0241: Post Search Bar
## Metadata
- Ticket Number: OCC-0241
- Ticket Title: Post Search Bar
- Story Points: 1
- Deadline: one hour from starting time
- Technologies used: HTML, CSS, JavaScript

### Preface
We've recently partnered with Company X and they wish to integrate with our platform. They have provided us with a [REST-based API](https://jsonplaceholder.typicode.com) and as part of our "Code Challenge" initiative, you have been tasked with building the search widget to consume that data. This widget will appear on the front page of our new integration one hour from now and in order to stay synced up with Marketing, _we can't miss this deadline._

### Getting Started
We've recently streamlined this process and as a result, our process has changed. To obtain a local copy of this repo, please run the following:

- `git clone https://gitlab.com/RaycatRakittra/oranj-code-challenges.git <your name without spaces>`
- `cd <your name without spaces>`

If you're already in the repo, run:

- `git checkout develop`

After doing this, you'll create another branch off of `develop`. At Oranj, we prefer the naming convention of `<feature|bug|hotfix|adhoc>/<ticket number>-<ticket summary>`. You don't have to go crazy with the `ticket summary` part; it just needs to be distinct enough. Once you've created your own branch, you'll be ready for development.

When committing, your commit message should contain the ticket number. If it doesn't, Git will throw an error during the pre-receive hook.

_When working off your feature branch, you may look at TIPS.md._

<hr/>

### Acceptance Criteria

*We're creating a search widget that looks through the `posts` provided by Company X and gives us the necessary data displayed in the staging area.*

Entire widget

- ... should adhere to the design provided
- ... should be easy to use

Search bar

- ... should be editable
- ... should search on either title or body

Search button

- ... should execute an action when clicked

Staging area

- ... should display the appropriate information after searching

### Nice-to-haves

Since we're working on such a tight timetable, we had to reduce the original scope of the project. If you are ahead of time, you may add any number of these features.

Entire widget

- ... can search on other endpoints like `comments`, `users`, and `todos`

Search bar

- ... has auto-completion functionality
- ... looks at both title and body for the search text

Staging area

- ... has a link per post that looks up that user's information

## Good luck!
